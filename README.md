# ExaHyPE

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-silver-silver)](https://eu.badgr.com/public/assertions/uhyUShBWT1Wfd0Vx3EoMrQ "SQAaaS silver badge achieved")
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.14536563.svg)](https://doi.org/10.5281/zenodo.14536563)

ExaHyPE is an engine to solve hyperbolic PDE systems. It uses high-order discontinuous Galerkin discretisation (DG,
with ADER time stepping) on tree-structured dynamically adaptive Cartesian meshes. Within ChEESE-2P special
emphasis is given to ExaSeis, the collection of seismic models in ExaHyPE (in particular seismic wave propagation and
dynamic rupture on curvilinear meshes), on shallow-water models and on ExaHyPE’s integration with the MIT
Uncertainty Quantification (MUQ) library for uncertainty quantification problems.

## Documentation


## Developers


## License and copyright

Copyright (c) 2016, The ExaHyPE Consortium, http://exahype.eu
All rights reserved.

Open Source License
===================

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
